<?php

namespace App\Model;


class User
{
    /**
     * @var Int
     */
    protected $id;

    /**
     * @var String
     */
    protected $pseudo;

    /**
     * @var String
     */
    protected $password;

    /**
     * @return Int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return String
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param String $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }

    /**
     * @return String
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param String $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

}