<?php

namespace App\Repository;

use App\Data\MySQLDataStore;
use App\Model\User;

class UserRepository
{

    private $db;

    public function __construct(MySQLDataStore $db)
    {
        $this->db = $db;
    }

    public function inscription($pseudo, $mdp, $confimMdp) {
        $errorString = "";
        
        if (strlen($mdp) < 6) {
            $errorString =  $errorString . "Mot de passe trop court.<br>";
        }
        if (strcmp($mdp, $confimMdp) !== 0) {
            $errorString =  $errorString . "Le mot de passe et la confirmation doivent être identique.<br>";
        }
        $userData = $this->db->select("SELECT idU as id FROM utilisateur WHERE pseudo = ? LIMIT 1", [$pseudo]);
        if (count($userData) > 0) {
            $errorString =  $errorString . "Le membre " . $pseudo . " existe déjà.<br>";
        }
        if (strcmp($errorString, "") === 0) {
            $options = [
                'cost' => 12,
            ];
            $this->db->insert("INSERT INTO utilisateur(pseudo, hash) VALUES (?, ?)",
            [
                $pseudo, password_hash(hash("sha256", $mdp, true), PASSWORD_DEFAULT, $options)
            ]);
        }

        return $errorString;
    }

    public function connexion($pseudo, $mdp) {
        $errorString = "";
        
        $userData = $this->db->select("SELECT idU as id, hash FROM utilisateur WHERE pseudo = ? LIMIT 1", [$pseudo]);
        if (count($userData) === 0) {
            $errorString =  $errorString . "Le membre " . $pseudo . " n'existe pas.<br>";
        } else {
            $user = $userData[0];
            if (password_verify(hash("sha256", $mdp, true), $user['hash']) === TRUE) {
                $_SESSION['id'] = $user['id'];
            } else {
                $errorString = "Mot de passe incorecte !<br>";
            }
        }       

        return $errorString;
    }



}
