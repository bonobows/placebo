# Installation
Installez [Docker et Docker-Compose](https://docs.docker.com/install/)

Générez les certificats SSL avec ` make `.

## macOS
Lancez la commande suivante afin d'ajouter le certificat précédemment généré dans le trouceau de mac.
```
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain apache/ssl/server.crt
```

## Linux/Windows
### Chrome
TO DO ([à tester](https://stackoverflow.com/a/15076602))

# Utilisation
```
docker-compose up -d --build
```

### Contraintes
php/js  
1 point d'entrée  
réécriture d'URL -> routeur  
template -> smarty  
bdd -> PDO  
