<div class="main">
  <h2>{$title}</h2>

  <form enctype="multipart/form-data" id="form-filter" class="filter">
    {if $connected}
    <label for="select-symptomes">Symptomes :
      <input id='select-symptomes' name="symptomes" class="form-input" data-multiple value="{$data->filters->symptome}"/>
    </label>
    {/if}
    <label for="meridien">Meridien :
      <select class="form-input" id="meridien" name="meridien">
        <option value="all">Tous</option>
        {foreach from=$data->meridiens item=meridien}
          <option value="{$meridien->getCode()}" {if $data->filters->meridien eq $meridien->getCode()}selected{/if}>{$meridien->getNom()}</option>
        {/foreach}
      </select>
    </label>
    <label for="typePatho">Type :
      <select class="form-input" id="typePatho" name="typePatho">
        {foreach from=$data->types item=type key=keyType}
          <option value="{$keyType}" {if $data->filters->type eq $keyType}selected{/if}>{$type}</option>
        {/foreach}
      </select>
    </label>
    <label for="caracteristique">Caractéristique :
      <select class="form-input" name="caracteristique" id="caracteristique">
        {foreach from=$data->caracteristiques item=caracteristique key=keyCaracteristique}
          <option value="{$keyCaracteristique}" {if $data->filters->caracteristique eq $keyCaracteristique}selected{/if}>{$caracteristique}</option>
        {/foreach}
      </select>
    </label>
    <button id="filter" class="button" type="submit" name="filter">Filtrer</button>
    <input id="order-by" type="hidden" name="order-by" value="meridien">
    <input id="sens" type="hidden" name="sens" value="asc">
  </form>
  <div id="ajax-result">
  </div>
</div>

<script src="/public/js/pathologie.js" type="text/javascript"></script>
