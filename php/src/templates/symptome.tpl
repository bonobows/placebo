<div class="main">
  <h2>{$title}</h2>
  {if $connected}
  <form id="form-filter" class="filter" method="get">
    <label for="keyword"><span>Mots-clés : </span>
      <input id="keyword" class="form-input" type="text" name="keyword" value="{$data->keyword}">
    </label>
    <button id="filter" class="button" type="submit">Chercher</button>
  </form>
  <div id="ajax-result">
    <table id="sympto-table" class="patho-table">
      <thead>
        <tr>
          <th>Symptomes <span id="desc-desc" class="order">&#9650;</span><span id="desc-ascend" class="order">&#9660;</span></th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {foreach from=$data->symptomes item=symptome}
        <tr>
          <td>{$symptome->getDescription()}</td>
          <td> <a aria-label="Examiner la pathologie" href="/Pathologies/symptome/{$symptome->getDescription()}"> <i title="Visualiser la pathologie" aria-hidden class="fas fa-eye"></i></a> </td>
        </tr>
        {/foreach}
      </tbody>
    </table>
  </div>
  {else}
    <p>Cette page est réservé aux utilisateurs connectés</p>
  {/if}
</div>

<script src="/public/js/symptomes.js" type="text/javascript"></script>
