<h2>{$data->pathologie->getDescription()}</h2>
<p>
  Meridien : {$data->pathologie->getMeridien()}
</p>
<p>
  Type : {$data->pathologie->getType()}
</p>
<h3>Liste des symptomes :</h3>
<ul class="visu-meridien">
  {foreach from=$data->symptomes item=symptome}
  <li>{$symptome->getDescription()}</li>
  {/foreach}
</ul>
