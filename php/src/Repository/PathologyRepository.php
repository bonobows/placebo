<?php

namespace App\Repository;

use App\Data\MySQLDataStore;
use App\Model\Pathology;

class PathologyRepository
{

    private $db;

    public function __construct(MySQLDataStore $db)
    {
        $this->db = $db;
    }

    public function findAll() {
        $pathologiesData = $this->db->select("SELECT idP as id, mer as meridien, type, `desc` as description FROM patho");
        $patholigies = array();
        foreach ($pathologiesData as $pathologyData) {
            $pathology = $this->map($pathologyData);
            array_push($patholigies, $pathology);
        }
        return $patholigies;
    }

    public function findAllReadable() {
      $pathologiesData = $this->db->select("SELECT idP as id, meridien.nom as meridien, type, `desc` as description FROM patho INNER JOIN meridien on patho.mer = meridien.code");
      $patholigies = array();
      foreach ($pathologiesData as $pathologyData) {
          $pathology = $this->map($pathologyData);
          array_push($patholigies, $pathology);
      }
      return $patholigies;
    }

    public function findById(Int $id) {
        $pathologyData = $this->db->select("SELECT idP as id, mer as meridien, type, `desc` as description FROM patho WHERE idP = ? LIMIT 1", [$id]);
        if (count($pathologyData) > 0) {
            return $this->map($pathologyData[0]);
        }
        return null;
    }

    public function findByIdReadable(Int $id) {
        $pathologyData = $this->db->select("SELECT idP as id, meridien.nom as meridien, type, `desc` as description FROM patho INNER JOIN meridien on patho.mer = meridien.code WHERE idP = ? LIMIT 1", [$id]);
        if (count($pathologyData) > 0) {
            return $this->map($pathologyData[0]);
        }
        return null;
    }

    public function insertOrUpdate(Pathology $pathology) {
        if ($pathology->getId() < 0) {
            return $this->db->insert("INSERT INTO patho (mer, type, `desc`) VALUES (?, ?, ?)",
                [
                    $pathology->getMeridien(),
                    $pathology->getType(),
                    $pathology->getDescription()
                ]);
        }
        return $this->db->insert("UPDATE patho SET mer=?, type=?, `desc`=? WHERE idP = ?",
            [
                $pathology->getMeridien(),
                $pathology->getType(),
                $pathology->getDescription(),
                $pathology->getId()
            ]);
    }

    /**
     * requête la base des pathologies d'acupuncture en fonction de différents filtre cf cahier des charges.
     * Filtres possibles :
     *       - Méridien (20 valeurs différentes) => meridien.nom
     *       - Type de pathologie (méridien, organe/viscère, luo, jing jin) => recherche de terme dans patho.desc
     *       - Caractèristique (plein, chaud, vide, froid, interne, externe) => recherche de terme dans patho.desc
     *
     * @param $nbParPage : nombre d'objets pathologie attendu au maximum (pagination)
     * @param $numPage : page attendu (va de paire avec $limit)
     * @param $meridien : filtre sur le méridien (string recherchée dans meridien.nom)
     * @param $typePatho : filtre sur le type de pathologie (string recherchée dans patho.desc)
     * @param $cara : filtre sur la caractèristique de pathologie (string recherchée dans patho.desc)
     *
     * @return $pathos tableau de max $nbParPage objets pathologies
     */
    public function requestWithFilter($nbParPage, $numPage, $meridien, $typePatho, $cara)
    {
        $offset = $nbParPage * ($numPage-1);

        if (strtolower($typePatho) == "toutes"){
            $typePatho = "";
        }
        if (strtolower($cara) == "toutes"){
            $cara = "";
        }

        if (strtolower($meridien) == "tous") {
            $query =
                "SELECT p.idP as id, p.desc as description, p.mer as meridien, p.type as type FROM patho p
                WHERE p.desc LIKE ? AND p.desc LIKE ?
                LIMIT ".$nbParPage." OFFSET ".$offset;
                $pathosData = $this->db->select($query, ["%".$typePatho."%", "%".$cara."%"]);
        } else {
            $query =
                "SELECT p.idP as id, p.desc as description, p.mer as meridien, p.type as type FROM patho p
                JOIN meridien m On p.mer = m.code
                WHERE m.nom = ? AND p.desc LIKE ? AND p.desc LIKE ?
                LIMIT ".$nbParPage." OFFSET ".$offset;
                $pathosData = $this->db->select($query, [$meridien, "%".$typePatho."%", "%".$cara."%"]);
        }
        $pathos = array();
        foreach ($pathosData as $pathologyData) {
            $pathology = $this->map($pathologyData);
            array_push($pathos, $pathology);
        }
        return $pathos;
    }

    /**
     * requête la base de pathologies d'acupuncture en se basant sur les mots clés possibles liés à des symptômes, eux même liés à des pathologies.
     * @param $nbParPage nb resultats retournés et à afficher dans la page
     * @param $numPage indice de la page à afficher (pagination)
     * @param $keyword valeur à trouver parmi les keywords.name de la base
     * @return $pathos tableau de max $nbParPage objets pathologies en page $numPage
     */
    public function getPathoFromKeyword($nbParPage, $numPage, $keyword){
        $offset = $nbParPage * ($numPage-1);
        $query =
            "SELECT p.idP as id, p.desc as description, p.mer as meridien, p.type as type FROM patho p
            INNER JOIN symptPatho sp On p.idP = sp.idP
            INNER JOIN symptome s On sp.idS = s.idS
            INNER JOIN keySympt ks On s.idS = ks.idS
            INNER JOIN keywords k On ks.idK = k.idK
            WHERE k.name LIKE ?
            LIMIT ".$nbParPage." OFFSET ".$offset;
        $pathosData = $this->db->select($query, ["%".$keyword."%"]);
        $pathos = array();
        foreach ($pathosData as $pathologyData) {
            $pathology = $this->map($pathologyData);
            array_push($pathos, $pathology);
        }
        return $pathos;
    }

    /**
     * requête la base des pathologies d'acupuncture en fonction de différents filtre cf cahier des charges.
     * Filtres possibles :
     *       - Méridien (20 valeurs différentes) => meridien.nom
     *       - Type de pathologie (méridien, organe/viscère, luo, jing jin) => recherche de terme dans patho.desc
     *       - Caractèristique (plein, chaud, vide, froid, interne, externe) => recherche de terme dans patho.desc
     *
     * @param $nbParPage (ex : 5) : nombre d'objets pathologie attendu au maximum (pagination)
     * @param $numPage (ex : 1) : page attendu (va de paire avec $limit)
     * @param $meridien (ex : E) : filtre sur le méridien
     * @param $typePatho (ex : voi luo) : filtre sur le type de pathologie (string recherchée dans patho.desc)
     *              Types possibles :
     *                   méridien
     *                   jing jin
     *                   fu
     *                   luo
     *                   grand luo
     *                   mervilleux vaisseaux
     * @param $cara (ex : vide) : filtre sur la caractèristique de pathologie (string recherchée dans patho.desc)
     *              Caractèristiques possibles :
     *                   externe
     *                   interne
     *                   plein
     *                   vide
     *                   chaud
     *                   froid
     *                   yin
     *                   yang
     *                   superieur
     *                   moyen
     *                   inferieur
     *                   posterieur
     *                   anterieur
     * @param $symptomesStr (ex : "Abdomen flasque;Bâillements") : String composée de symptome.desc séparés par des ";" et qui concerne les pathologies voulues
     *
     * @return $pathos tableau de max $nbParPage objets pathologies
     */
    public function searchPathos($nbParPage = 25, $numPage = 1, $meridien = "", $typePatho = "", $cara = "", $symptomesStr = "")
    {
        //init
        $offset = $nbParPage * ($numPage-1);
        $symptomes = explode("; ", $symptomesStr);

        //Do a IN clause only if there are symptomes
        $mysqlInStr = "";
        if ($symptomes[0] != null){ //because explode return by default an empty first element
            $in  = str_repeat('?,', count($symptomes) - 1) . '?'; //PDO is so bad with "IN" statment...
            $mysqlInStr = "AND s.desc IN (".$in.") ";
        }
        //handle merveilleux vaisseau case (samer laput)
      if (strpos($typePatho, 'merve') !== false) {
          $typeFilter = "p.type LIKE ?";
          $typePatho = "mv";
      } else {
          $typeFilter = "p.desc LIKE ?";
      }

        //Build the very very big query da
        $query =
            "SELECT DISTINCT SQL_CALC_FOUND_ROWS p.idP as id, p.desc as description, m.nom as meridien, p.type as type FROM patho p
            INNER JOIN symptPatho sp On p.idP = sp.idP
            INNER JOIN symptome s On sp.idS = s.idS
            INNER JOIN meridien m on m.code = p.mer
            WHERE  p.mer Like ? AND ".$typeFilter." AND p.desc LIKE ? ".$mysqlInStr.
            "LIMIT ".$nbParPage." OFFSET ".$offset;

        if ($symptomes[0] != null){
            $pathosData = $this->db->select($query, array_merge(["%".$meridien."%", "%".$typePatho."%", "%".$cara."%"], $symptomes));
        }else{
            $pathosData = $this->db->select($query, ["%".$meridien."%", "%".$typePatho."%", "%".$cara."%"]);
        }

        //treat result and return as php array
        $pathos = array();
        foreach ($pathosData as $pathologyData) {
            $pathology = $this->map($pathologyData);
            array_push($pathos, $pathology);
        }
        return $pathos;
    }

    public function getLastRow(){
      $data = $this->db->select("SELECT FOUND_ROWS()");
      return $data[0][0];
    }

    private function map(array $data) {
        $pathology = new Pathology();
        $pathology->setId($data['id']);
        $pathology->setDescription($data['description']);
        $pathology->setMeridien($data['meridien']);
        $pathology->setType($data['type']);
        return $pathology;
    }
}
