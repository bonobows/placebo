<?php

namespace App\Model;


class Pathology
{

    /**
     * @var Int
     */
    protected $id = -1;

    /**
     * @var Meridien
     */
    protected $meridien;

    /**
     * @var String
     */
    protected $type;

    /**
     * @var String
     */
    protected $description;

    /**
     * @return Int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return String
     */
    public function getMeridien()
    {
        return $this->meridien;
    }

    /**
     * @param String $meridien
     */
    public function setMeridien($meridien)
    {
        $this->meridien = $meridien;
    }

    /**
     * @return String
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param String $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return String
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

}