<nav id="menu">
  <ul>
    <li {if $active eq 'pathologie'}class='active'{/if}><a accesskey="p" title="Liste des pathologies. (p)" href="/Pathologies">Pathologies</a></li>
    <li {if $active eq 'about'}class='active'{/if}><a accesskey="a" title="À propos de ce site. (a)" href="/About">À propos</a></li>
    {if !$connected}
      <li {if $active eq 'inscription'}class='active'{/if}><a accesskey="i" title="Inscription. (i)" href="/Inscription">Inscription</a></li>
      <li {if $active eq 'connexion'}class='active'{/if} class="hidden-750"><a accesskey="c" title="Connectez-vous. (c)" href="/Connexion">Connexion</a></li>
    {else}
      <li {if $active eq 'symptome'}class='active'{/if}><a accesskey="s" title="Liste des symptomes. (s)" href="/Symptomes">Symptomes</a></li>
      <li><a accesskey="d" title="Deconnexion. (d)" href="/Deconnexion">Deconnexion</a></li>
    {/if}
  </ul>
</nav>
