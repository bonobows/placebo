window.onload = () => {
  getTable(true);
}

document.getElementById('form-filter').addEventListener('submit', (e) => {
  e.preventDefault();
  getTable();
  return false;
})

Array.from(document.getElementsByClassName('order'))
  .forEach(el => el.addEventListener('click', (ev) => {
    let arg = ev.target.id.split('-');
    document.getElementById('order-by').value = arg[0];
    document.getElementById('sens').value = arg[1];
    getTable();
  }))

function getTable(onload = false, page = 0) {
  let form = document.getElementById('form-filter');
  var data = new FormData(form);

  // console.log(data.get('order-by') + '-' + data.get('sens'));

  let suffix = '';
  if (data.get('symptomes')) {
    suffix += `/symptome/${data.get('symptomes').replace(/; ?$/,'')}`
  }
  if (data.get('meridien')) {
    suffix += `/meridien/${data.get('meridien')}`
  }
  if (data.get('typePatho')) {
    suffix += `/type/${data.get('typePatho')}`
  }
  if (data.get('caracteristique')) {
    suffix += `/caracteristique/${data.get('caracteristique')}`
  }
  if (page !== 0) {
    suffix += `/page/${page}`;
  } else {
    let isPage = false;
    window.location.href.split('/').forEach((url) => {
      if (url === 'page') {
        isPage = true;
      } else if (isPage) {
        suffix += `/page/${url}`;
      }
    });
  }
  if (!onload) {
    window.history.pushState('pathologie', 'Pathologies', `/Pathologies${suffix}`)
  }
  fetch(`/api/pathologies${suffix}`)
    .then((res) => {
      // console.log('result ajax', res);
      // document.getElementById('ajax-result').innerHTML = res;
      return res.text();
    }).then(html => {
      // console.log('text', html);
      document.getElementById('ajax-result').innerHTML = html;
    }).catch((err) => {
      console.error('failed ajax', err);
    });
}

var selectSymptomesInput = document.getElementById("select-symptomes");
let selectSymptomes = new Awesomplete(selectSymptomesInput, {
  minChars: 0,
  filter: function(text, input) {
    return Awesomplete.FILTER_CONTAINS(text, input.match(/[^;]*$/)[0]);
  },

  item: function(text, input) {
    return Awesomplete.ITEM(text, input.match(/[^;]*$/)[0]);
  },

  replace: function(text) {
    var before = this.input.value.match(/^.+;\s*|/)[0];
    this.input.value = before + text + "; ";
  }
});

document.getElementById('select-symptomes').addEventListener("input", () => {
  let search = document.getElementById('select-symptomes').value.split('; ');
  fetch(`/api/symptomes/search/${search[search.length - 1]}`)
    .then((res) => {
      return res.json();
    }).then((data) => {
      selectSymptomes.list = data.map(symp => symp.description);
    }).catch((err) => {
      console.error('failed ajax', err);
    });
});

function changePage(page) {
  getTable(false, page);
  return false;
}
