<?php
namespace App;

use App\Data\MySQLDataStore;
use App\Repository\PathologyRepository;
use App\Repository\SymptomeRepository;
use App\Repository\MeridienRepository;
use App\Repository\UserRepository;
use Smarty;

require_once 'vendor/autoload.php';
$smarty = new Smarty();
$db = new MySQLDataStore();
$pathologyRepo = new PathologyRepository($db);
$symptomeRepo = new SymptomeRepository($db);
$meririenRepo = new MeridienRepository($db);
$userRepo = new UserRepository($db);


$data = new \stdclass;
$smarty->template_dir	= './templates/';
$smarty->config_dir	= './templates/configs/';
$smarty->compile_dir	= './templates/templates_c/';
$smarty->cache_dir	= './templates/cache/';

session_start();
if (!empty($_SESSION['msg'])) {
    $data->msg = $_SESSION['msg'];// htmlspecialchar ?
    $data->type = $_SESSION['type'];
    $_SESSION['msg'] = "";
    $_SESSION['type'] = "";
}

//Si le membre est connecté
if (!empty($_SESSION['id'])) {
    $smarty->assign("connected", "true");
}

$page = strtolower(htmlspecialchars($_GET['page']));
if ($page === "pathologies") {
    $data->pathologies = $pathologyRepo->findAllReadable();
    $data->meridiens = $meririenRepo->findAll();
    $data->types = array("all"=>"Tous", "meridien"=>"Méridien", "fu"=>"Organe/viscère", "luo"=>"Luo", "Merveilleux vaisseaux"=>"Merveilleux vaisseaux", "jing jin"=>"Jing jin");
    $data->caracteristiques = array("all"=>"Tous", "plein"=>"Plein", "chaud"=>"Chaud", "vide"=>"Vide", "froid"=>"Froid", "interne"=>"Interne", "externe"=>"Externe");
    //print_r($meridiens);
    if (isset($_GET['filter']) && $_GET['filter'] !== '') {
        $data->filters = new \stdClass();
        $filters = htmlspecialchars($_GET['filter']);
        $filterList = explode('/', $filters);
        $i = 0;
        while ($i < count($filterList)) {
            if ($filterList[$i] === "symptome") {
                $data->filters->symptome = $filterList[$i + 1] . '; ';
            } elseif ($filterList[$i] === "meridien") {
                $data->filters->meridien = $filterList[$i + 1];
            } elseif ($filterList[$i] === "type") {
                $data->filters->type = $filterList[$i + 1];
            } elseif ($filterList[$i] === "caracteristique") {
                $data->filters->caracteristique = $filterList[$i + 1];
            }
            $i += 2;
        }
    }
    $smarty->assign("title", "Pathologies");
    $smarty->assign("tpl_name", "pathologie");
} elseif ($page === "apropos") {
    $smarty->assign("title", "À propos");
    $smarty->assign("tpl_name", "about");
}  elseif ($page === "error") {
    $smarty->assign("title", "Erreur 404");
    $smarty->assign("tpl_name", "error");
} elseif ($page === "connexion") {
    if ($_POST) {
        $erorsMsg = $userRepo->connexion(
            htmlspecialchars($_POST['username']),
            htmlspecialchars($_POST['password'])
        );
        if (strcmp($erorsMsg, "") !== 0) {
            $data->msg = $erorsMsg;
            $data->type = "error";
        } else {
            $_SESSION['msg'] = "Connexion réussie.";
            $_SESSION['type'] = "success";
            header('Location: /Pathologies');
            exit();
        }
    }
    $smarty->assign("title", "Connexion");
    $smarty->assign("tpl_name", "connexion");
} elseif ($page === "deconnexion") {
    session_destroy();
    header('Location: /Pathologies');
    exit();
} elseif ($page === "inscription") {
    if ($_POST) {
        $erorsMsg = $userRepo->inscription(
            htmlspecialchars($_POST['username']),
            htmlspecialchars($_POST['passwordCofirm']),
            htmlspecialchars($_POST['password'])
        );
        if (strcmp($erorsMsg, "") !== 0) {
            $data->msg = $erorsMsg;
            $data->type = "error";
        } else {
            $_SESSION['msg'] = "Inscription réussie.";
            $_SESSION['type'] = "success";
            header('Location: /Connexion');
            exit();
        }
    }
    $smarty->assign("title", "Inscription");
    $smarty->assign("tpl_name", "inscription");
} elseif ($page === "pathologie") {
    $data->pathologie = $pathologyRepo->findByIdReadable($_GET['id']); //Faille SQL ?
    $symptomes = $symptomeRepo->findByIdPatho($_GET['id']);
    $data->symptomes = $symptomes; //Faille SQL ?
  $smarty->assign("title", "Visualisation patho"); // TODO -> to description
  $smarty->assign("tpl_name", "visuPatho");
} elseif ($page === "symptomes") {
    if (isset($_GET['keyword']) && $_GET['keyword'] !== '') {
        $data->keyword = htmlspecialchars($_GET['keyword']);
        $data->symptomes = $symptomeRepo->findByKeyword($data->keyword);
    }
    $smarty->assign("title", "Symptomes");
    $smarty->assign("tpl_name", "symptome");
}


if ($page === "api") {
    if (isset($_GET['api'])) {
        $apiQuery = strtolower(htmlspecialchars($_GET['api']));
        $apiQueryList = explode('/', $apiQuery);
        //symptomes
        if (strcmp($apiQueryList[0], 'symptomes') ==0) {
            //search symptomes
            if (strcmp($apiQueryList[1], 'search') ==0) {
                echo json_encode($symptomeRepo->findBySearch($apiQueryList[2]));
            }
        }
        //pathologies
        if ($apiQueryList[0] === 'pathologies') {
            // echo "oui";
            $nbParPage = 25;
            $numPage = 1;
            $meridien = "";
            $typePatho = "";
            $cara = "";
            $symptomesStr = "";
            $i = 1;
            while (isset($apiQueryList[$i])) {
                if (strcmp($apiQueryList[$i], "symptome") === 0) {
                    $symptomesStr = $apiQueryList[$i + 1];
                } elseif (strcmp($apiQueryList[$i], "meridien") === 0 && strcmp($apiQueryList[$i + 1], "all") !== 0) {
                    $meridien = $apiQueryList[$i + 1];
                } elseif (strcmp($apiQueryList[$i], "type") === 0 && strcmp($apiQueryList[$i + 1], "all") !== 0) {
                    $typePatho = $apiQueryList[$i + 1];
                } elseif (strcmp($apiQueryList[$i], "caracteristique") === 0 && strcmp($apiQueryList[$i + 1], "all") !== 0) {
                    $cara = $apiQueryList[$i + 1];
                } elseif (strcmp($apiQueryList[$i], "page") === 0) {
                    $numPage = $apiQueryList[$i + 1];
                }
                $i += 2;
            }
            $pathologies = $pathologyRepo->searchPathos($nbParPage, $numPage, $meridien, $typePatho, $cara, $symptomesStr);
            $lastPage = floor($pathologyRepo->getLastRow() / $nbParPage) + 1;
            // $data->maxSize = floor($pathologyRepo->getLastRow() / $nbParPage) + 1;
            $smarty->assign("pathologies", $pathologies);
            $smarty->assign("lastPage", $lastPage);
            $smarty->assign("actualPage", $numPage);
            $smarty->display('./templates/api/pathologies-result.tpl');
        }
    }
} else {
    $smarty->assign("sitename", "Plac' & Bonobo");
    $smarty->assign("data", $data);
    $smarty->display('./templates/structure.tpl');
}
