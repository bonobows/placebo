<header>
  <div class="header-top">
    <div class="header-comp">
      <a href="/"><img src="/public/images/mask.jpg" alt="masque d'acupuncture" id="logo" /></a>
      <div id="burger" class="burger hidden-550">
        <div class="line line-top"></div>
        <div class="line line-center"></div>
        <div class="line line-bottom"></div>
      </div>
      <div class="title">
        <h1>{$sitename}</h1>
        <cite>La référence acupuncture</cite>
      </div>
    </div>
    {if !$connected}
      <form class="connexion" action="Connexion" method="post">
        <label for="username">Nom d'utilisateur</label>
        <input class="form-input" type="text" id="username" name="username" value="">
        <label for="password">Mot de passe</label>
        <input class="form-input" type="password" id="password" name="password" value="">
        <input class="button" type="submit" name="connexion" value="Connexion">
      </form>
    {/if}
  </div>
  {include file='../components/menu.tpl' active="$active" connected="$connected"}
</header>
