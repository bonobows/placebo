<?php

namespace App\Data;

use PDO;

class MySQLDataStore
{
    private $db;

    public function __construct()
    {
        $user = getenv("MYSQL_USER");
        $password = getenv("MYSQL_PASSWORD");
        $dbname = getenv("MYSQL_DATABASE");
        $host = getenv("MYSQL_HOST");
        try {
            $this->db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $user, $password);
        } catch (\PDOException $e) {
            print "Erreur : " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function __destruct()
    {
        $this->db = null;
    }

    public function select(String $statement, array $data = null) {
        $sth = $this->db->prepare($statement); //instead of pdo::quote()
        $sth->execute($data);
        return $sth->fetchAll();
    }

    public function insert(String $statement, array $data = null) {
        $sth = $this->db->prepare($statement);
        return $sth->execute($data);
    }
}