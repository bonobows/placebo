var burger = document.getElementById('burger');
var isOpen = false;
if(burger){
  burger.addEventListener("click", () => {
    if(isOpen){
      document.getElementById('menu').classList.remove("menu-show");
      burger.classList.remove("menu-show");
      isOpen = false;
    } else {
      document.getElementById('menu').classList.add("menu-show");
      burger.classList.add("menu-show");
      isOpen = true;
    }
  });
}
