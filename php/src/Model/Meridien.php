<?php

namespace App\Model;
use JsonSerializable;


class Meridien implements JsonSerializable
{
    /**
     * @var String
     */
    protected $code;

    /**
     * @var String
     */
    protected $nom;

    /**
     * @var String
     */
    protected $element;

    /**
     * @var Boolean
     */
    protected $yin;

    /**
     * @return String
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param String $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return String
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param String $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return String
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * @param String $element
     */
    public function setElement($element)
    {
        $this->element = $element;
    }

    /**
     * @return bool
     */
    public function isYin()
    {
        return $this->yin;
    }

    /**
     * @param bool $yin
     */
    public function setYin($yin)
    {
        $this->yin = $yin;
    }

    public function jsonSerialize() {
      return [
        'code' => $this->getCode(),
        'nom' => $this->getNom(),
        'element' => $this->getElement(),
        'yin' => $this->isYin()
      ];
    }
}
