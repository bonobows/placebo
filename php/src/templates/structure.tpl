<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>{$title} - {$sitename}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="/public/css/style.css">
    <link rel="stylesheet" href="/public/css/mobile.css">
    <link rel="stylesheet" media="print" href="/public/css/printer.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link rel="stylesheet" href="/public/css/awesomplete.css" />
    <script src="/public/js/awesomplete.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    {include file='./components/header.tpl' title="$sitename" active="$tpl_name" connected="$connected"}
    <main>
      {if (isset($data->type))}
      <div class="{$data->type}">
        {$data->msg}
      </div>
      {/if}
      {include file="$tpl_name.tpl" data=$data connected=$connected}
    </main>
    {include file='./components/footer.tpl'}
  </body>

  <script src="/public/js/script.js" type="text/javascript"></script>
</html>
