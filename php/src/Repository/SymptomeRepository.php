<?php

namespace App\Repository;

use App\Data\MySQLDataStore;
use App\Model\Symptom;

class SymptomeRepository
{

    private $db;

    public function __construct(MySQLDataStore $db)
    {
        $this->db = $db;
    }

    public function findByIdPatho(Int $idPatho) {
      $symptomesData = $this->db->select("SELECT DISTINCT s.* FROM symptome s INNER JOIN symptPatho sp on sp.idS = s.idS WHERE sp.idP = ?", [$idPatho]);
      // if (count($symptomeData) > 0) {
      //     return $this->map($symptomeData);
      // }
      $symptomes = array();
      foreach ($symptomesData as $symptomeData) {
          $symptome = $this->map($symptomeData);
          array_push($symptomes, $symptome);
      }
      return $symptomes;
    }

    public function findBySearch(String $s){
      $symptomesData = $this->db->select("SELECT DISTINCT * FROM `symptome` WHERE `desc` like ?", ['%' . $s . '%']);
      // if (count($symptomeData) > 0) {
      //     return $this->map($symptomeData);
      // }
      $symptomes = array();
      foreach ($symptomesData as $symptomeData) {
        $symptome = $this->map($symptomeData);
        array_push($symptomes, $symptome);
      }
      return $symptomes;
    }

    public function findByKeyword(String $keyword) {
      $symptomesData = $this->db->select("SELECT s.* FROM symptome s INNER JOIN keySympt ks ON s.idS = ks.idS INNER JOIN keywords k on ks.idK = k.idK WHERE k.name LIKE ?", ['%' . $keyword . '%']);
      $symptomes = array();
      foreach ($symptomesData as $symptomeData) {
        $symptome = $this->map($symptomeData);
        array_push($symptomes, $symptome);
      }
      return $symptomes;
    }

    public function insertOrUpdate(Symptom $symptome) {
        if ($symptome->getId() < 0) {
            return $this->db->insert("INSERT INTO patho (`desc`) VALUES (?, ?, ?)",
                [
                    $symptome->getDescription()
                ]);
        }
        return $this->db->insert("UPDATE patho SET `desc`=? WHERE idP = ?",
            [
                $symptome->getDescription(),
                $symptome->getId()
            ]);
    }

    private function map(array $data) {
        $symptome = new Symptom();
        $symptome->setId($data['idS']);
        $symptome->setDescription($data['desc']);
        return $symptome;
    }
}
