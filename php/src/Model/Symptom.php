<?php

namespace App\Model;
use JsonSerializable;


class Symptom implements JsonSerializable {

    /**
     * @var Int
     */
    protected $id;

    /**
     * @var String
     */
    protected $description;

    /**
     * @return Int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return String
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function jsonSerialize() {
      return [
        'id' => $this->getId(),
        'description' => $this->getDescription()
      ];
    }

}
