<div class="about">
    <h2>A propos de ce site</h2>
        <h3>Cahier des charges</h3>
            <p>
                L’association des acupuncteurs soucieux de l’accessibilité (AAA...) nous a sollicité pour la conception
                et la réalisation d’une plateforme en ligne dont les principales fonctionnalités seraient :
                <ol>
                    <li>de disposer d’un service en ligne leur permettant de consulter la liste des symptômes des principales pathologies en acupuncture ;</li>
                    <li>de pouvoir n’afficher que certaines des pathologies en fonction de différents critères (type de pathologie, choix des méridiens) ;</li>
                    <li>de rechercher les pathologies comportant certains symptômes.</li>
                </ol>
            </p>
        <h3>Développements</h3>
            <h4>Web statique et accessibilité</h4>
                <!--TODO : validateur auto, structuration ??, elements supp ?, responsive, impression-->
                <h5>HTML et CSS</h5>
                <p>La structure du code <abbr title="HyperText Markup Language">HTML</abbr> correspond au standard <abbr title="HyperText Markup Language">HTML</abbr> 5 (un header, footer, ...).
                Le <abbr title="HyperText Markup Language">HTML</abbr> est décorrélé du <abbr title="Cascade Style Sheet">CSS</abbr> et du JavaScript puisque ceux-ci sont dans des fichiers disctints.</p>
                <p>Afin que le client puisse imprimer le résultat d'une recherche, nous avons mis en place un <abbr title="Cascade Style Sheet">CSS</abbr> pour l'impression.
                <a href="/public/css/printer.css">Celui-ci</a> se contente de masquer tous les éléments superflus (header, footer, ...).</p>
            <h4>Web dynamique / Web services et architecture REST</h4>
                <!--TODO : routing, structuration des scripts, model mvc smarty-->
                <h5>Moteur de template</h5>
                <p>
                    Nous avons choisi d'utiliser un moteur de template pour pourvoir mettre en place une architecture MVC et ainsi découpler la logique de la présentation et rendre le code plus lisible et maintenable. Smarty qui nous a été imposé, répondais simplement à ce besoin.
                </p>
                <h5>AJAX</h5>
                <p>
                        Afin de dynamiser certaines fonctionnalités du site, nous avons choisi d'utiliser l'architecture <abbr title="Asynchronous JavaScript And XML">AJAX</abbr> pour communiquer avec le serveur.</br>
                        Nous utilisons cette architecture pour récupérer les symptomes et le tableau des pathologies.</br>
                </p>
                <h5>Web Service</h5>
                <p>
                        Pour pouvoir récuprer ces données, nous avons mis en place un webservice rest sur le serveur.</br>
                        Cette api expose deux url : <a>/api/symptomes/</a> et <a>/api/pathologies/</a>.</br>
                        La première url retourne un format json contentant la liste des symptomes recherché à l'aide d'une mot clé.</br>
                        La seconde url retourne du texte html contenant le tableau des pathologies filtrés.
                </p>
                <h5>SQL</h5>
                <p>
                    Les pathologies sont affichées grace à une fonction php qui requête la base de données. Elle accepte en paramètre les indications de pagination, ainsi que les différentes valeures de tri correspondantes aux filtres proposés dans la page front. </br></br>
                    Le cas où l'on ne veut pas filtrer sur les symptômes est supporté, la requête peut donc prendre différentes formes. </br>
                    Elle est composée de plusieurs INNER JOIN pour parcourir les différentes tables. </br>
                    Enfin on utilise les méthodes de <abbr title="PHP Data Objects">PDO</abbr> pour préparer et effecter la requête ce qui permet d'éviter les injections <abbr title="Structured Query Language">SQL</abbr>.</br>
                </p>
            <h4>Pipelines</h4>
                <p>
                    L'une des principales demandes du client était l'accessibilité du site. 
                    Afin de tester automatiquement l'accessibilité, nous avons ajouté un outil qui permet de tester certains critères, pa11y.
                    Ces critères sont issus des différents niveaux des Web Content Accessibility Guidelines 2.0.
                    Nous avons ainsi choisi de le configurer avec le niveau AAA, le plus élevé.
                </p>
                <p>
                    Pour vérifier que le site respectait toujours les critères définis, nous avons mis en place des pipelines sur Gitlab qui permettent de compiler et de tester un projet.
                    Ses pipelines peuvent être déclenchés manuellement ou bien lors d'une merge request.
                    Nous avons donc fait en sorte que lorsque le pipeline qui était déclenché par une merge request retournait une erreur (car l'un des critères n'était pas respecté), le merge n'était pas possible.
                    Il faut alors résoudre la (ou les) erreur(s) pour pouvoir fusionner.
                </p>
            <h4>Réseau</h4>
                <!--TODO : docker, architecture, documentation ??-->
        <h3><!--TODO : (technos, pipeline ?)-->Sources</h3>
                <ul>
                    <li><a href="http://php.net/">PHP</a></li>
                    <li><a href="https://www.smarty.net/docsv2/fr/">Smarty</a></li>
                    <li><a href="https://www.mysql.com/fr/">MySQL</a></li>
                    <li><a href="https://www.docker.com/">Docker</a></li>                    
                </ul>
        <h3><!--TODO : -->Webographie</h3>
            <ul>
                <li><a href="http://php.net/manual/en/book.pdo.php">PDO</a></li>
                <li><a href="https://dev.mysql.com/">Doc MySQL</a></li>
                <li><a href="https://www.w3schools.com/">W3School</a></li>                    
            </ul>
</div>