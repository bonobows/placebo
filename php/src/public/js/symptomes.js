document.getElementById('form-filter').addEventListener('submit', event => {
  event.preventDefault();
  window.open(`/Symptomes/${document.getElementById('keyword').value}`, '_self')
  return false;
});

document.getElementById('desc-desc').addEventListener('click', event => {
  var tbody = document.querySelector('#sympto-table tbody'),
    tr = document.querySelectorAll('#sympto-table tbody tr');
  var paraArr = [].slice.call(tr).sort((trA, trB) => {
    let trAstr = trA.children[0].innerText;
    let trBstr = trB.children[0].innerText;
    if (trAstr < trBstr) return -1;
    if (trAstr > trBstr) return 1;
    return 0;
  });
  paraArr.forEach((p) => {
    tbody.appendChild(p);
  });
  document.getElementById('desc-ascend').classList.remove("active");
  event.target.classList.add("active");
});

document.getElementById('desc-ascend').addEventListener('click', event => {
  var tbody = document.querySelector('#sympto-table tbody'),
    tr = document.querySelectorAll('#sympto-table tbody tr');
  var paraArr = [].slice.call(tr).sort((trA, trB) => {
    let trAstr = trA.children[0].innerText;
    let trBstr = trB.children[0].innerText;
    if (trAstr < trBstr) return 1;
    if (trAstr > trBstr) return -1;
    return 0;
  });
  paraArr.forEach((p) => {
    tbody.appendChild(p);
  });
  document.getElementById('desc-desc').classList.remove("active");
  event.target.classList.add("active");
});
