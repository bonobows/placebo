<?php

namespace App\Repository;

use App\Data\MySQLDataStore;
use App\Model\Meridien;

class MeridienRepository
{

    private $db;

    public function __construct(MySQLDataStore $db)
    {
        $this->db = $db;
    }

    public function findAll(){
      $meridiensData = $this->db->select("SELECT * FROM `meridien` ORDER BY nom");

      $meridiens = array();
      foreach ($meridiensData as $meridienData) {
          $meridien = $this->map($meridienData);
          array_push($meridiens, $meridien);
      }
      return $meridiens;
    }

    private function map(array $data) {
        $symptome = new Meridien();
        $symptome->setCode($data['code']);
        $symptome->setNom($data['nom']);
        $symptome->setElement($data['element']);
        $symptome->setYin($data['yin']);
        return $symptome;
    }
}
