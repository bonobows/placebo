{if $pathologies|@count eq 0 }
<p> aucune pathologie pour cette recherche</p>
{else}
<table class="patho-table">
  <thead>
    <tr>
      <th>Meridien</th>
      <th>Type</th>
      <th>Description</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$pathologies item=pathologie}
    <tr>
      <td>{$pathologie->getMeridien()}</td>
      <td>{$pathologie->getType()}</td>
      <td>{$pathologie->getDescription()}</td>
      <td> <a aria-label="Examiner la pathologie" href="/Pathologies/{$pathologie->getId()}"> <i title="Visualiser la pathologie" aria-hidden class="fas fa-eye"></i></a> </td>
    </tr>
    {/foreach}
  </tbody>
</table>
<div class="paginate">
  <a {if $actualPage != 1}  onclick="return changePage({$actualPage - 1})"{else} class="paginate-disable"{/if}>&lt;</a>
  {for $page=1 to $lastPage}
    <a {if $actualPage eq $page} class="paginate-active"{/if}  accesskey="{$page}" title="Aller à la page {$page}. ({$page})" onclick="return changePage({$page})">{$page}</a>
  {/for}
  <a {if $actualPage != $lastPage} onclick="return changePage({$actualPage + 1})"{else} class="paginate-disable"{/if}>&gt;</a>
</div>
{/if}
